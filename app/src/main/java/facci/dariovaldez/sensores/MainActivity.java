package facci.dariovaldez.sensores;

import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btnVibrar, btnAcelerometro, btnProximidad, btnSensorLuz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnVibrar = (Button) findViewById(R.id.btnVibrar);
        btnAcelerometro = (Button) findViewById(R.id.btnAcelerometro);
        btnProximidad = (Button) findViewById(R.id.btnProximidad);
        btnSensorLuz = (Button) findViewById(R.id.btnSensorLuz);

        final Vibrator vibrator = (Vibrator)this.getSystemService(Context.VIBRATOR_SERVICE);

        btnVibrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrator.vibrate(1000);
            }
        });

        btnAcelerometro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AcelerometroActivity.class);
                startActivity(intent);
            }
        });

        btnProximidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ProximidadActivity.class);
                startActivity(intent);
            }
        });

        btnSensorLuz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LuzActivity.class);
                startActivity(intent);
            }
        });

    }
}
